
//Lab Exercise 3

#include <iostream>
#include <conio.h>
#include <fstream>

using namespace std;

string CompleteMadlib(string madlib[], string madlibtemplate[], const int &SIZE);

int main()
{
	const int SIZE = 12;

	string madlib[SIZE];
	madlib[0] = "adjective";
	madlib[1] = "sport";
	madlib[2] = "city";
	madlib[3] = "person";
	madlib[4] = "verb (past tense)";
	madlib[5] = "vehicle";
	madlib[6] = "place";
	madlib[7] = "noun (plural)";
	madlib[8] = "adjective";
	madlib[9] = "food (plural)";
	madlib[10] = "liquid";
	madlib[11] = "adjective";

	string madlibtemplate[SIZE + 1];
	madlibtemplate[0] = "One day my ";
	madlibtemplate[1] = " friend and I decided to go to the ";
	madlibtemplate[2] = " game in ";
	madlibtemplate[3] = ".\nWe really wanted to see ";
	madlibtemplate[4] = " play.\nSo we ";
	madlibtemplate[5] = " in the ";
	madlibtemplate[6] = " and headed down to ";
	madlibtemplate[7] = " and bought some ";
	madlibtemplate[8] = ".\nWe watched the game and it was ";
	madlibtemplate[9] = ".\nWe ate some ";
	madlibtemplate[10] = " and drank some ";
	madlibtemplate[11] = ".\nWe had a ";
	madlibtemplate[12] = " time, and can't wait to go again.";

	
	for (int i = 0; i < 12; i++)
	{
		cout << "\nEnter a(n) " << madlib[i] << ": ";
		cin >> madlib[i];
	}

	string madlibcomplete = CompleteMadlib(madlib, madlibtemplate, SIZE);
	cout << "\n" << madlibcomplete;

	char yn = '0';
	cout << "\n\nWould you like to save the madlib to a file? (y/n):";
	cin >> yn;
	if (yn == 'y' || yn == 'Y')
	{
		string filepath = "Madlib.txt";
		ofstream ofs(filepath);
		ofs << madlibcomplete;
		ofs.close();
		cout << "Madlib has been saved to " << "Madlib.txt" << ".\nPress any key to exit...";
	}
	else
	{
		cout << "Madlib not saved.\nPress any key to exit...";
	}

	(void)_getch();
	return 0;
}

string CompleteMadlib(string madlib[], string madlibtemplate[], const int &SIZE)
{
	string madlibcomplete = "";
	for (int i = 0; i < SIZE; i++)
	{
		madlibcomplete = madlibcomplete + madlibtemplate[i] + madlib[i];
		if (i == (SIZE - 1))
		{
			madlibcomplete = madlibcomplete + madlibtemplate[SIZE];
		}
	}
	return madlibcomplete;
}